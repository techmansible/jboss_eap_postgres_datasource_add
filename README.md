jboss_eap_postgres_datasource_add
=========

Copy postgres jdbc dirvers to EAP server


Call this role from main play:
=========

  roles:

    - role: jboss_eap_postgres_datasource_add
      postgresql_datasource_name: "{{ pruquote_postgresql_datasource_name }}"
      postgres_connection: "{{ pruquote_postgres_connection }}"
      postgres_username: "{{ pruquote_postgres_username }}"
      postgres_password: "{{ pruquote_postgres_password }}"
  
  vars_files:
    - config.yml
	
Here, the values are defined in group_vars of the main play under respective group name